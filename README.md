## Требования
* Debian 11 / Ubuntu 22.04
* Установленные nginx, docker и docker compose
* 1 GB RAM + хотя бы пару ядер CPU для нормальной работы
* аккаунт на cloudflare

## Установка
1. Создайте не рутового пользователя у себя на сервере `adduser nullchan && usermod -a -G sudo nullchan && usermod -aG docker nullchan && su nullchan`
2. Склонируйте этот репозиторий в любую папку (например, `/home/nullchan/instant-0chan-docker`)
3. Склонируйте инстант в папку app (например, `/home/nullchan/instant-0chan-docker/app`)
4. Зайдите в папку instant-0chan-docker (`cd instant-0chan-docker`)
5. Скопируйте .env.example с новым именем .env (`cp .env.example .env`)
6. Скопируйте конфиг nginx (не забудьте указать в нем свой домен вместо 0chan.tld) `sudo cp nginx/0chan.tld.conf /etc/nginx/sites-available/`
7. Поменяйте `server_name` в скопированном файле и создайте симлинк для активации домена (`sudo ln -s /etc/nginx/sites-available/0chan.tld.conf /etc/nginx/sites-enabled/`)
8. Проверим что ничего не поломалось (`sudo nginx -t`)
9. Если всё ок, то перезагружаем nginx (`sudo nginx -s reload`)
10. Возвращаемся к файлу `.env`
11. Обязательно поменяйте `MARIADB_PASSWORD` и `MARIADB_ROOT_PASSWORD` (иначе я лично приду и снесу вам базу)
12. Можно запустить `docker compose build && docker compose up -d`
13. Когда всё установится, вам вернется управление терминалом. Вводим команду `docker ps` чтобы удостовериться что всё поднялось. Запоминаем хэш контейнера    `kusaba-docker-app`, например у меня он `cd9ea837d6b4`
14. Выполняем команду `docker exec -it cd9ea837d6b4 bash` (с вашим айди контейнера)
15. `cp app/instance-config.php.example cp app/instance-config.php`
16. Отредактируем `instance-config.php`, установим настройки базы (обратите внимание что в качестве хоста используем не localhost а `mariadb`)
17. `mkdir -p /var/www/dwoo/templates_c` на всякий случай
18. `chown -R www-data:www-data /var/www/app`, это необходимо чтобы были права на создание кэша шаблонов и аплоада картинок
19. Ctrl+D, чтобы выйти из контейнера
20. Выполняем установку инстанта по инструкции самого инстанта
21. Подключаем сайт в настройках dns cloudflare
22. Устанавливаем certbot по инструкции с официального сайта
23. Выполняем `sudo certbot --nginx`, следуем инструкции, получаем сертификат для нашего сайта

Если у вас что-то не получилось, отпишитесь в треде https://0chan.plus/0/res/1799.html